package com.pawelbanasik;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Api {

	private static String ua = "Pawel/1.0";

	// Zad 1a - jak argument mapa a zwraca Stringa jako QueryUri
	public String makeQueryURI(HashMap<String, String> map) {

		String queryUri = "";

		for (String key : map.keySet()) {

			queryUri += key + "=" + map.get(key) + "&";

		}

		return queryUri.substring(0, queryUri.length() - 1);

	}

	// zad 1b - jako argument daje Stringa a uzyskuje HashMap
	public HashMap<String, String> makeQueryURI(String queryURI) {

		HashMap<String, String> map = new HashMap<>();

		String[] elements = queryURI.split("&");
	
		String key = "";
		String value = "";
		for (int i = 0; i < elements.length; i++) {

			key = (elements[i].split("="))[0];
			value = (elements[i].split("="))[1];
			
			map.put(key, value);
		}
		return map;
	}

	// Zad 2 - wysylamy zdarzenia post ktore sprawdzaja loginy z listy co do statusu
	public void checkLoginStatuses(List<String> logins, String url) throws IOException {

		for (int i = 0; i < logins.size(); i++) {

			// tworzy nowego urla
			URL obj = new URL(url);
			
			// tworzy nowe polaczenie do tego urla zmienna typu HttpURLConnection
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// te String w nawiasie to jest konwencja i tak musza byc wpisane
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", ua);

			String myParams = "";
			myParams = "login=" + logins.get(i);
			
			// uzywam do outputa wiec flaga na true
			con.setDoOutput(true);
			
			DataOutputStream dos = new DataOutputStream(con.getOutputStream());
			dos.writeBytes(myParams);
			dos.flush();
			dos.close();
			
			String ret = "";
			if (con.getResponseCode() == 200) {
				Scanner s = new Scanner(new InputStreamReader(con.getInputStream()));
				while (s.hasNextLine())
					ret += s.nextLine();
				s.close();
			}
			
			if (ret.contains("ok"))
				System.out.println(logins.get(i) + " OK ");
			else
				System.out.println(logins.get(i) + " ERROR ");
		}
	}

	// zad 3 (nie zrobione)
	public static void sendGET(List<String> address, String url) throws IOException {

		// definujemy adres polaczenia (hosta)
		// String url = "http://wp.pl";

		// definujemy przegladrke (user-agent)

		// definiujemy obiekt typu url - odwolujemy sie pod wskazany adres;

		for (int i = 0; i < address.size(); i++) {

			URL obj = new URL(address.get(i));

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", ua);

			String ret = "", currentLine = "";

			if (con.getResponseCode() == 200) {
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((currentLine = br.readLine()) != null) {
					ret += currentLine;
				}
				br.close();
			}

			System.out.println(ret);
		}
	}
}
