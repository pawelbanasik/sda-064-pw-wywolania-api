package com.pawelbanasik;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) throws IOException {

		// HashMap<String, String> map = new HashMap<>();
		//
		// map.put("klucz", "wartosc");
		// map.put("drugiklucz", "drugawartosc");
		//
		// // mozna od razu iterowac po keySet lub zrobic zmienna
		// // Set <String> keys = map.keySet();
		//
		// for (String key : map.keySet()) {
		// System.out.println("Dla klucza: " + key + " wartosc wynosi: " +
		// map.get(key));
		// }

		// zadanie 1b

		// HashMap<String, String> map = new HashMap<>();
		//
		// map.put("klucz", "wartosc");
		// map.put("drugiklucz", "drugawartosc");
		//
		// Api api = new Api();
		//
		// System.out.println(api.makeQueryURI(map));

		// zadanie 1b

		// Api api = new Api();
		// HashMap<String, String> map =
		// api.makeQueryURI("klucz=wartosc&drugiklucz=drugawartosc");
		//
		// for (String key : map.keySet()) {
		// System.out.println("Klucz: " + key + ", Wartosc: " + map.get(key));
		// }

		// zadanie 2

		Api api = new Api();

		List<String> logins = Arrays.asList("tester", "administrator", "admin", "oper", "test", "pawel", "superadmin",
				"operator");

		try {
			api.checkLoginStatuses(logins, "http://palo.ferajna.org/sda/wojciu/json.php");

		} catch (IOException e) {
			e.printStackTrace();
		}

		
	}
	//
	// // Zad 3
	// List<String> addresses = Arrays.asList("164.12.33.45", "174.12.33.45",
	// "127.0.0.127", "172.217.23.46",
	// "192.168.12.34", "185.11.130.82", "104.192.143.3", "145.237.193.133");

}
